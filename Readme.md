Task-1: Host a Chyrp-lite application on the nginx server 

•	Step-1: Install the nginx server  
o	Command: “yum install nginx”
o	Start the nginx server: “systemctl start nginx”
 
•	Step-2: Install the php
o	Command: “yum install php-fpm php-mysqlnd”
o	Check the version: “php –version”

•	Step-3: Enable php-fpm service
o	Command: “systemctl enable php-fpm.service”
o	Start the service: “systemctl start php-fpm.service”
 
•	Step-4: Create a project folder under /usr/share/nginx/html/ directory with chyrplite                  name 
o	Command: “mkdir /usr/share/nginx/html/chyrplite”
o	Paste your project here.
 
•	Step-5: Create a virtual host file under the /etc/nginx/conf.d folder and file name must be chyrplite.com.conf
o	Command: “nano /etc/nginx/conf.d/chyrplite.com.conf”
o	Paste this line of code

server {
#       listen 85 default_server;
#       listen [::]:85 default_server;

        server_name chyrplite.com www.chyrplite.com;
        root /usr/share/nginx/html/chyrplite;
        index index.html index.php index.htm index.nginx-debian.html;

        location / {
                try_files $uri $uri/ =404;
        }
}
•	Step-6: Install MySql server and configure that.
o	Command: “yum install mariadb-server”
o	Start the server: “systemctl start mariadb”
o	Enter the mysql databse: “mysql”
o	Create a database: “CREATE DATABASE example_database;”
o	Create a user for database: “GRANT ALL ON example_database.* TO 'example_user'@'localhost' IDENTIFIED BY 'password' WITH GRANT OPTION;”
o	Show the database: “SHOW DATABASES;” 
 
•	Step-7: Open app on the browser 
o	Command: “hostip:hostport/chyrplite/install.php”
o	Install the database by adding the valuses.

